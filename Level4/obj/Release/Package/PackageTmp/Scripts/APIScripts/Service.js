﻿app.service("LibraryService", function ($http) {


    this.getMembers = function () {
        return $http.get("http://localhost:21662/api/Library")
    }

    this.saveMember = function (member) {
        return $http({
            method: 'post',
                data: member,
                url: 'http://localhost:21662/api/Library'
        });
    }


    this.getBooks = function () {
        return $http.get("http://localhost:21662/api/Book")
    }

    this.saveBook = function (book) {
        return $http({
            method: 'post',
            data: book,
            url: 'http://localhost:21662/api/Book'
        });
    }
})   