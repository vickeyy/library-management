﻿app.controller('MemberController', function ($scope, LibraryService) {
    getAllMembers();

    function getAllMembers() {
        var servCall = LibraryService.getMembers();
        servCall.then(function (d) {
            $scope.members = d.data;
        }, function (error) {
            console.log('Oops! Something went wrong while fetching the data.');
        })
    }

    $scope.saveMemb = function () {
        var member = {
            member_FirstName: $scope.fname,
            member_LastName: $scope.lname,
            member_Gender: $scope.gender,
            member_Age: $scope.age,
            //member_DateJoin: new Date(),
            member_Address: $scope.address
        };
        var saveMemb = LibraryService.saveMember(member);
        saveMemb.then(function (d) {
            getAllMembers();

        }, function (error) {
            console.log('Oops, error');
        })

    };
})

app.controller('BookController', function ($scope, LibraryService) {
    getAllBooks();

    function getAllBooks() {
        var servCall = LibraryService.getBooks();
        servCall.then(function (d) {
            $scope.books = d.data;
        }, function (error) {
            console.log("Oops, something went wrong");
        })
    }

    $scope.saveBook = function () {
        var book = {
            book_Title: $scope.title,
            book_Author: $scope.author,
            book_Pages: $scope.pages,
            book_DateOfPublish: $scope.publishdate
        };
        var saveBook = LibraryService.saveBook(book);
        saveBook.then(function (d) {
            getAllBooks();
        }, function (error) {
            console.log('oops,error');
        })
    };
})





