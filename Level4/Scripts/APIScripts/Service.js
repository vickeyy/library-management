﻿app.service("LibraryService", function ($http) {


    this.getMembers = function () {
        return $http.get("http://localhost:21662/api/Library")
    }


    this.getMemberbyID = function (member_ID) {
        return $http({
            method: 'get',
            url: 'http://localhost:21662/api/Library/'+ member_ID

        });
    }

    this.saveMember = function (member) {
        return $http({
            method: 'post',
                data: member,
                url: 'http://localhost:21662/api/Library'
        });
    }


    this.deleteMember = function (member_ID) {
        return $http(
            {
                method: 'delete',
                data: member_ID,
                url: 'http://localhost:21662/api/Library/' + member_ID
            });
    }


    this.getBooks = function () {
        return $http.get("http://localhost:21662/api/Book")
    }

    this.saveBook = function (book) {
        return $http({
            method: 'post',
            data: book,
            url: 'http://localhost:21662/api/Book'
        });
    }

    this.getTitlebyID = function (book_ID) {
        return $http({
            method: 'Get',
            data: book_ID,
            url: 'http://localhost:21662/api/Book/' + book_ID
        });
    }



    this.deleteBook = function (book_ID) {
        return $http(
            {
                method: 'delete',
                data: book_ID,
                url: 'http://localhost:21662/api/Book/' + book_ID
            });
    }

    this.getTransactions = function () {
        return $http({
            method: 'Get',
            url: 'http://localhost:21662/api/Transaction'
        });
    }

    this.saveTransaction = function (trans) {
        return $http({
            method: 'POST',
            data: trans,
            url: 'http://localhost:21662/api/Transaction'
        });
    }

    this.updateTransaction = function (trans) {
        return $http({
            method: 'PUT',
            data: trans,
            url: 'http://localhost:21662/api/Transaction/'+ trans.ID
        });
    }

    this.deleteTransaction = function (id) {
        return $http({
            method: 'delete',
            data: id,
            url: 'http://localhost:21662/api/Transaction/'+ id
        });
    }

})


