﻿app.controller('MemberController', function ($scope, LibraryService) {
    getAllMembers();



    function getAllMembers() {
        var servCall = LibraryService.getMembers();
        servCall.then(function (d) {
            $scope.members = d.data;
        }, function (error) {
            console.log('Oops! Something went wrong while fetching the data.');
        })
    }




    $scope.saveMemb = function () {
        var member = {
            member_FirstName: $scope.fname,
            member_LastName: $scope.lname,
            member_Gender: $scope.gender,
            member_Age: $scope.age,
            //member_DateJoin: new Date(),
            member_Address: $scope.address
        };
        var saveMemb = LibraryService.saveMember(member);
        saveMemb.then(function (d) {
            getAllMembers();

        }, function (error) {
            console.log('Oops, error')
        })

    };

    $scope.delMember = function (member_ID) {
        var dlt = LibraryService.deleteMember(member_ID);
        dlt.then(function (d) {
            getAllMembers();
        }, function (error) {
                console.log('Oops! Something went wrong while deleting the data.')
            })
    };


})

app.controller('BookController', function ($scope, LibraryService) {
    getAllBooks();

    function getAllBooks() {
        var servCall = LibraryService.getBooks();
        servCall.then(function (d) {
            $scope.books = d.data;
        }, function (error) {
            console.log("Oops, something went wrong");
        })
    }


    $scope.saveBook = function () {
        var book = {
            book_Title: $scope.title,
            book_Author: $scope.author,
            book_Pages: $scope.pages,
            book_DateOfPublish: $scope.publishdate
        };
        var saveBook = LibraryService.saveBook(book);
        saveBook.then(function (d) {
            getAllBooks();
        }, function (error) {
            console.log('oops,error')
        })
    };



    $scope.delBook = function (book_ID) {
        var dlt = LibraryService.deleteBook(book_ID);
        dlt.then(function (d) {
            getAllBooks();
        }, function (error) {
            console.log('Oops! Something went wrong while deleting the data.')
        })
    };

})

app.controller('TransactionController', function ($scope,$rootScope, LibraryService) {
    getAllTransactions();
    var member_name = "";
    var member_id = 0;
    var book_ID = 0;
    var book_title = "";
  // $scope.isDisabled = false;
    //var book_dateOfIssue = new Date;
    function getAllTransactions() {
        var servCall = LibraryService.getTransactions();
        servCall.then(function (d) {
            $scope.transactions = d.data;
        }, function (error) {
            console.log("Oops, something went wrong");
        })
    }

    $scope.getName = function () {
        var value = $scope.mid;
        var getName = LibraryService.getMemberbyID(value);
        getName.then(function (d) {
            $scope.user = d.data;
            $rootScope.member_id = d.data.member_ID;
            $rootScope.member_name = d.data.member_FirstName + d.data.member_LastName;
        }, function (error) {
            console.log('Something went wrong');
        }
        )
    };
    $scope.getTitle = function () {
        var value = $scope.bid;
        var getTitle = LibraryService.getTitlebyID(value);
        getTitle.then(function (d) {
            $scope.Book = d.data;
            $rootScope.book_id = d.data.book_ID;
            $rootScope.book_title = d.data.book_Title;
        }, function (error) {
            console.log('Something went wrong');
        }
        )
    };

    $scope.saveTrans = function () {
        var trans = {
            member_ID: $rootScope.member_id,
            member_Name: $rootScope.member_name,
            book_ID: $rootScope.book_id,
            book_Title: $rootScope.book_title
        };
        var saveTrans = LibraryService.saveTransaction(trans);
        saveTrans.then(function (d) {
            getAllTransactions();
        }, function (error) {
            console.log('Something went wrong');
        })
    };

    $scope.updateTrans = function (trans) {
       // $scope.isDisabled = true;
        var updateTrans = LibraryService.updateTransaction(trans);
        updateTrans.then(function () {
            getAllTransactions();
        }, function (error) {
            console.log('Something went wrong');
        })
    };

    $scope.deleteTrans = function (id) {
        var deleteTrans = LibraryService.deleteTransaction(id);
        deleteTrans.then(function () {
            getAllTransactions();
        }, function (error) {
            console.log('Something went wrong');
        })
    };

})
