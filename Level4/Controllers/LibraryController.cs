﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Level4.Models;
namespace Level4.Controllers
{
    public class LibraryController : ApiController
    {
        private LibraryEntities library = new LibraryEntities();


        [HttpGet]
        public IEnumerable<Member> Get()
        {
            return library.Members.AsEnumerable();

        }

        [HttpGet]
        public Member Get(int id)
        {
            var found = library.Members.FirstOrDefault(e => e.member_ID == id);
            if (found != null)
                return found;
            else
                return null;

        }




        [HttpPost]
        public HttpResponseMessage Post(Member memb)
        {
            try
            {
                memb.member_DateJoin = DateTime.Today.ToShortDateString();

                library.Members.Add(memb);
                library.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.Created, "Member Added");
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            var to_Delete = library.Members.FirstOrDefault(e => e.member_ID == id);
            if (to_Delete != null)
            {
                library.Members.Remove(to_Delete);
                library.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK, to_Delete.member_FirstName.ToString() + " " + to_Delete.member_LastName.ToString() + " deleted");
            }
            return Request.CreateResponse(HttpStatusCode.NotFound, "No member found");
        }






    }
}
