﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Level4.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
        
        public ActionResult Books()
        {
            ViewBag.Title = "Book List";
            return View();
        }

        public ActionResult Members()
        {
            ViewBag.Title = "Member List";
            return View();
        }

        public ActionResult Transaction()
        {
            ViewBag.Title = "Transaction List";
            return View();
        }
    }

}
