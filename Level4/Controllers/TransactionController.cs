﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Level4.Models;
using System.Data.Entity;
namespace Level4.Controllers
{
    public class TransactionController : ApiController
    {
        private LibraryEntities transaction = new LibraryEntities();

        [HttpGet]
        public IEnumerable<Transaction> Get()
        {
            return transaction.Transactions.AsEnumerable();
        }

        [HttpPost]

        public HttpResponseMessage Post(Transaction process)
        {
            if (process.dateOfIssue == null)
                process.dateOfIssue = DateTime.Today;
            transaction.Transactions.Add(process);
            transaction.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.Created, "Process recorded");

        }

        [System.Web.Http.HttpPut]
        public HttpResponseMessage Put(Transaction found)
        {
            if (found.dateOfReturn != null ||found==null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotAcceptable, "Transaction already closed, or not found");
            }
            found.dateOfReturn = DateTime.Today;
            int diff = (int)(DateTime.Today - found.dateOfIssue).Value.TotalDays;
            found.daysIssued = diff;
            if (diff > 15)
                found.fine = (diff - 15) * 10;
            transaction.Entry(found).State = EntityState.Modified;
            transaction.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, "Transaction closed");
        }


    [HttpDelete]
    public HttpResponseMessage Delete(int id)
    {
            Transaction trans = transaction.Transactions.FirstOrDefault(e => e.ID == id);
            if (trans != null)
            {
                transaction.Transactions.Remove(trans);
                transaction.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, trans);
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Provided transaction not found");
    }




}
}

