﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Level4.Models;
namespace Level4.Controllers
{
    public class BookController : ApiController
    {
        private LibraryEntities library = new LibraryEntities();
        

        [HttpGet]
        public IEnumerable<Book> Get()
        {
            return library.Books.AsEnumerable();
        }

        [HttpGet]

       public Book Get(int id)
        {
            var found = library.Books.FirstOrDefault(e => e.book_ID == id);
            if (found != null)
                return found;
            return null;
        }

        [HttpPost]
        public HttpResponseMessage Post(Book book)
        {
            try
            {
                library.Books.Add(book);
                library.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.Created, "Book Added");
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }

        [HttpDelete]

        public HttpResponseMessage Delete(int id)
        {

            var to_Delete = library.Books.FirstOrDefault(e => e.book_ID == id);
            if (to_Delete != null)
            {
                library.Books.Remove(to_Delete);
                library.SaveChanges();
             
                return Request.CreateResponse(HttpStatusCode.OK, to_Delete.book_Title.ToString() + " by " + to_Delete.book_Author.ToString() + " deleted");
            }
            return Request.CreateResponse(HttpStatusCode.NotFound, "No member found");
        }
    }
}
